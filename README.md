#### Secure messaging scorecard/Decentralized communication survey

A comparison of all the secure, private, [free-as-in-beer, free-as-in-freedom,](https://en.wikipedia.org/wiki/Gratis_versus_libre#%22Free_beer%22_vs_%22freedom_of_speech%22_distinction) decentralized, open-source communication social networks, apps, programs and similar services out there in the search for the Holy Grail(s, ideally). A merging of a recent [blockchain SN survey](https://gitlab.com/qqn/dctrl/blob/master/Decentralized_Social_Networks_and_the_Blockchain.pdf) (itself stemming from my [cryptocurrency research](https://spmx.ca/btc)) with an old thread on [Telegram's security](https://diasp.org/posts/9090114), an older post analyzing [decentralized SNs](https://networkcultures.org/unlikeus/2012/12/07/breaking-down-the-walls-by-paul-sulzycki-unlike-us-reader), the EFF's (outdated) [Secure Messaging Scorecard](https://www.eff.org/node/82654) (and its [quasi-update](https://www.eff.org/deeplinks/2018/03/secure-messaging-more-secure-mess)), and current [Federation/Fediverse applications](https://medium.com/we-distribute/a-quick-guide-to-the-free-network-c069309f334)... and a small [Diaspora post](https://diasp.org/posts/10578326) discussing all this.

|	#	|	Service	|	Mobile	|	Desktop	|	Browser	|	Android	|	PC	|	Mac	|	Linux	|	Private	|	Secure	|	Decentralized	|	Federating	|
|	:-:	|	:-	|	:-:	|	:-:	|	:-:	|	:-:	|	:-:	|	:-:	|	:-:	|	:-:	|	:-:	|	:-:	|	:-:	|
|	1	|	[Akasha](https://akasha.world/)	|		|		|		|		|		|		|		|		|		|		|		|
|	2	|	[Basic Attention Token](https://basicattentiontoken.org/)	|		|		|		|		|		|		|		|		|		|		|		|
|	3	|	[Bitmessage](https://bitmessage.org/)	|		|	✓	|	NO	|	✓	|	✓	|		|	✓	|	✓	|	✓	|	✓	|	NO	|
|	4	|	[BitTube](https://bit.tube/)	|		|		|		|		|		|		|		|		|		|		|		|
|	5	|	[Brair](https://briarproject.org/)	|		|		|		|		|		|		|		|		|		|		|		|
|	6	|	[Diaspora](https://diasporafoundation.org/)	|	[Dandelion](https://f-droid.org/en/packages/com.github.dfa.diaspora_android/)|	|	✓	|	✓	|	✓	|	✓	|	✓	|	✓	|		|		|	✓	|	✓	|
|	7	|	[Discord](https://discordapp.com)	|	✓	|	✓	|	✓	|	✓	|	✓	|	✓	|	✓	|		|		|		|		|
|	8	|	[Everipedia](https://everipedia.org/)	|		|		|	✓	|		|		|		|		|		|		|		|		|
|	9	|	[Friendica](https://friendi.ca/)	|		|	✓	|	✓	|		|	✓	|	✓	|	✓	|		|		|	✓	|	✓	|
|	10	|	[Gab](https://gab.com/)	|		|		|		|		|		|		|		|		|		|		|		|
|	11	|	[GNU social/StatusNet](https://www.gnu.org/software/social/)	|		|		|		|		|		|		|		|		|		|		|		|
|	12	|	[Hellofriend](https://joinhellofriend.com/)	|		|		|		|		|		|		|		|		|		|		|		|
|	13	|	[Hike](https://hike.in/)	|		|		|		|		|		|		|		|		|		|		|		|
|	14	|	[Hubzilla](https://project.hubzilla.org)	|		|		|		|		|		|		|		|		|		|	✓	|	✓	|
|	15	|	[Indorse](https://indorse.io/)	|		|		|		|		|		|		|		|		|		|		|		|
|	16	|	[Keybase](https://keybase.io/)	|	✓	|	✓	|		|	✓	|	✓	|	✓	|	✓	|	✓	|	✓	|		|	✓	|
|	17	|	[Kik](https://www.kik.com/)	|		|		|		|		|		|		|		|		|		|		|		|
|	18	|	[LBRY](https://lbry.io/)	|	beta	|	✓	|		|	beta	|		|		|		|		|		|		|		|
|	19	|	[Libertree](http://libertree.org/)	|		|	✓	|	✓	|		|	✓	|	✓	|	✓	|		|		|		|		|
|	20	|	[Mastodon](https://mastodon.social/)	|	[Tusky](https://tuskyapp.github.io/)|	|	✓	|	✓	|	✓	|	✓	|	✓	|	✓	|		|		|	✓	|	✓	|
|	21	|	[Minds](https://www.minds.com/)	|		|		|		|		|		|		|		|		|		|		|		|
|	22	|	[Nexus](https://nexusearth.com/)	|		|		|		|		|		|		|		|		|		|		|		|
|	23	|	[Obsidian](https://osm.obsidianplatform.com/)	|		|		|		|		|		|		|		|		|		|		|		|
|	24	|	[Ono](https://www.ono.chat/)	|		|		|		|		|		|		|		|		|		|		|		|
|	25	|	[OpenBazaar](https://openbazaar.org/)	|		|		|	✓	|		|		|		|		|		|		|		|		|
|	26	|	[Peepeth](https://peepeth.com/welcome)	|		|		|		|		|		|		|		|		|		|		|		|
|	27	|	[Primas](https://primas.io/)	|		|		|		|		|		|		|		|		|		|		|		|
|	28	|	[Props](https://propsproject.com/)	|		|		|		|		|		|		|		|		|		|		|		|
|	29	|	[Pump.io](http://pump.io/)	|		|		|		|		|		|		|		|		|		|		|		|
|	30	|	[Retroshare](http://retroshare.net/)	|		|		|		|		|		|		|		|		|		|		|		|
|	31	|	[Ricochet](https://ricochet.im/)	|	✓	|	✓	|	NO	|	✓	|	✓	|	✓	|	✓	|	✓	|	✓	|	✓	|	NO	|
|	32	|	[Ring](https://ring.cx/)	|	✓	|	✓	|	NO	|	✓	|	✓	|	✓	|	✓	|	✓	|	✓	|	✓	|	NO	|
|	33	|	[Riot/Matrix](https://matrix.org/docs/projects/client/riot.html)	|	✓	|	✓	|		|	✓	|	✓	|	✓	|	✓	|	✓	|	✓	|	✓	|		|
|	34	|	[Sapien](https://www.sapien.network/)	|		|		|		|		|		|		|		|		|		|		|		|
|	35	|	[SecureScuttlebutt](https://www.scuttlebutt.nz/)	|	[Manyverse](https://www.manyver.se/)|	|	✓	|		|	✓	|	✓	|	✓	|	✓	|		|		|	✓	|	NO	|
|	36	|	[Signal](https://signal.org/)	|	✓	|	✓	|		|	✓	|	✓	|	✓	|	✓	|	✓	|		|	NO	|	NO	|
|	37	|	[Socialhome](https://socialhome.network/)	|		|		|		|		|		|		|		|		|		|		|		|
|	38	|	[Sola](https://sola.ai/)	|		|		|		|		|		|		|		|		|		|		|		|
|	39	|	[SoMee](https://somee.social/)	|		|		|		|		|		|		|		|		|		|		|		|
|	40	|	[Steemit](https://steemit.com/)	|		|		|	✓	|		|		|		|		|		|		|		|		|
|	41	|	[Synereo](https://synereo.com/)	|		|		|		|		|		|		|		|		|		|		|		|
|	42	|	[Telegram](https://telegram.org/)	|	✓	|	✓	|		|	✓	|	✓	|	✓	|	✓	|	NO	|		|	NO	|	NO	|
|	43	|	[Tox](https://tox.chat/)	|		|		|		|		|		|		|		|		|		|		|		|
|	44	|	[Upfiring](https://upfiring.com/)	|		|		|		|		|		|		|		|		|		|		|		|
|	45	|	[Wickr](https://wickr.com/)	|		|		|		|		|		|		|		|		|		|		|		|
|	46	|	[Wire](https://wire.com/)	|		|		|		|		|		|		|		|		|		|		|		|
|	47	|	[Y'alls](https://yalls.org/)	|		|		|		|		|		|		|		|		|		|		|		|
|	48	|	[Yours](https://www.yours.org/)	|		|		|		|		|		|		|		|		|		|		|		|

**Scores**: blank (unknown), beta (working on it), ✓ (score!), NO (fail)  
*Secure*: Encrypted decentralized and/or local data storage  
*Private*: Default encrypted across ALL channels  
*Decentralized*: Distributed across many servers, ideally P2P  
*Federating*: Communicating with at least one other service on this list  

Popular services not included: anything from Facebook/Google/Microsoft (eg. Instagram, WhatsApp, Gmail/Gchat/Hangouts/Allo, Skype, GitHub, LinkedIn) or WeChat because [surveillance capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism), anything with a paywall/block (Pinterest, LinkedIn again), anything requiring payment (eg: [Threema](https://threema.ch) or [freemium](https://en.wikipedia.org/wiki/Freemium) services like [Slack](https://slack.com/), where old messages are locked up and released only after buying a subscription).

To do:
1. Add sources for all table scores
2. Figure out where XMPP/Jabber/Pidgin/IRC/[Conversations](https://conversations.im/)/[ChatSecure](https://chatsecure.org/) (and PGP) stands in this list
3. Include services like [Appear.in](https://appear.in/), [Cisco Spark/Webex](https://teams.webex.com/signin), [Jitsi Meet](https://meet.jit.si/), or [Talky](https://talky.io/) (or defunct services lik [Firefox Hello](https://support.mozilla.org/en-US/kb/hello-status))?
4. Include characteristics from https://www.picflash.org/viewer.php?img=p9NCOGXNV05TY.png
5. Where do filesharing services like [IPFS](https://ipfs.io/), [Peergos](https://peergos.org/) and [ownCloud](https://owncloud.org/) fit?
6. Integrate information from https://secushare.org/comparison, https://wiki.c3d2.de/EDN, https://en.wikipedia.org/wiki/Comparison_of_software_and_protocols_for_distributed_social_networking, https://en.wikipedia.org/wiki/Distributed_social_network, and https://www.w3.org/blog/news/archives/3958